#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>


int main (int argc, char *argv[])
{

	int fd;
	char mensaje [100];
	mkfifo ("tub.fifo", 0777);

	fd=open ("tub.fifo", O_RDONLY);
	while (1)
	{
		read (fd, mensaje, sizeof(char) *100);
		if (mensaje [0]=='c')
			break;
		puts(mensaje);
	}

	close(fd);
	unlink("tub.fifo");
	return (1);
}

